// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';

import { Button, Row, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom';


export default function Banner(props) 
{

    // deconstructing the array
    const {header, paragraph, buttonMessage} = props;

    // determine link based on button message
    const link = buttonMessage === "Back to Home" ? "/" : "";

    return (
        <Row>
        	<Col className="p-5">
                <h1>{header}</h1>
                <p>{paragraph}</p>
                <Link to={link}><Button variant="primary">{buttonMessage}</Button></Link>
            </Col>
        </Row>
    	)
}