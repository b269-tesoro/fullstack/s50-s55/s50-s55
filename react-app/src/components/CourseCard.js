import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';


// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';
// [S50 ACTIVITY END]


// [S50 ACTIVITY]
export default function CourseCard({course}) {

	// deconstruct the course properties into their own variables
	const {name, description, price, _id} = course;

	/*
	SYNTAX:
	const [getter, setter] = useState(initialGetterValue);
	*/
// 	const [count, setCount] = useState(0);
// // S51 ACTIVITY START
// 	const [seats, setSeats] = useState(30);

// 	function enroll ()
// 	{
// 		// if (seats >0)
// 		// {
// 			setCount(count + 1);
// 			setSeats(seats -1);
// 		// }
// 		// else
// 		// {
// 		// 	alert("no more seats.");
// 		// }
		
// 	}
// // S51 ACTIVITY END

// useEffect(() =>
// {
// 	if( seats <=0)
// 	{
// 		alert("No more seats available!");
// 	}
// },[seats]);

return (
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                        {/*<Card.Subtitle>Count: {count}</Card.Subtitle>*/}
                        {/*<Button variant="primary" onClick ={enroll} disabled={seats<=0}>Enroll</Button>*/}
                        <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
                    </Card.Body>
                </Card>
            </Col>
    </Row>        
    )
}
// [S50 ACTIVITY END]

/*S50 - MY ACTIVITY SOLUTION
import {Row, Col, Card, Button} from "react-bootstrap"

function CourseCard() {
  return (
  	<Row className="mt-3 mb-3">
  		<Col xs={12}>
		    <Card>
		      <Card.Body>
		        <Card.Title>Sample Course</Card.Title>
		        <Card.Text>
		          	Description:
				</Card.Text>
				<Card.Text>
		          	This is a sample course offering.
				</Card.Text>
				<Card.Text>
		          	Price:
				</Card.Text>
				<Card.Text>
		          	PHP 40,000
				</Card.Text>
		        <Button variant="primary">Enroll</Button>
		      </Card.Body>
		    </Card>
	    </Col>
	</Row>
  );
}

export default CourseCard;*/