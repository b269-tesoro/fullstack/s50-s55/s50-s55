const bannerData = [
    {
        id: "home",
        header: "Zuitt Coding Bootcamp",
        paragraph: "Opportunities for everyone, everywhere.",
        buttonMessage: "Enroll now!"
    },
    {
        id: "error",
        header: "Error 404 - Page not found.",
        paragraph: "The page you are looking for cannot be found.",
        buttonMessage: "Back to Home"
    }
]

export default bannerData;