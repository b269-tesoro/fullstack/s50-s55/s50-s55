import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';

import {Navigate, useNavigate} from 'react-router-dom';

import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Register() {

    const {user} = useContext(UserContext);
    const navigate = useNavigate();

    // to store values of the input fields
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNumber, setMobileNumber] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

useEffect(() => {
    if((firstName !== "" && lastName !== "" && email !== "" && mobileNumber !== "" && mobileNumber.length >= 11 && password1 !== "" && password2) && (password1 === password2)) {
        setIsActive(true);
    } else {
        setIsActive(false);

    }
}, [firstName, lastName,email, mobileNumber, password1, password2]);

    // function to simulate user registration
    function registerUser(e) {
        // Prevents page from reloading       
        e.preventDefault();

        // checking if the registered user already exists.
        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`,
        {
        	method: "POST",
        	headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        }).then(res => res.json())
        .then(data =>
        {
        	console.log(data);

        	if (data === true)
        	{
        		Swal.fire({
                    title: "Duplicate Email found",
                    icon: "error",
                    text: "Please provide a different email."
                })
        	}
        	else
        	{
        		fetch(`${process.env.REACT_APP_API_URL}/users/register`,
        		{
        			method: "POST",
        			headers: {
        		        'Content-Type': 'application/json'
        		    },
        		    body: JSON.stringify({
        		        firstName: firstName,
		        		lastName: lastName,
		        		email: email,
		        		mobileNo: mobileNumber,
		        		password: password1
        		    })
        		}).then(res => res.json())
        		.then(data =>
        		{
        			console.log(data);

        			Swal.fire({
	                    title: "Registration successful",
	                    icon: "success",
	                    text: "Welcome to Zuitt!"
                	})

                	navigate("/login");

        		})
        	}
        })
        // Clear input fields
        setEmail("");
        setPassword1("");
        setPassword2("");

        // alert('Thank you for registering!');
    };

    return (
        (user.id !== null)?
        <Navigate to ="/courses" />
        :
        <Form onSubmit={(e) => registerUser(e)}>
        	<Form.Group controlId="firstName">
        	        <Form.Label>First Name</Form.Label>
        	        <Form.Control 
        	            type="text" 
        	            placeholder="first name" 
        	            value={firstName}
        	            onChange={e => setFirstName(e.target.value)}
        	            required
        	        />
        	    </Form.Group>

        	<Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="last name" 
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="mobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="09123456789" 
                    pattern="[0-9]*"
        			inputMode="numeric"
                    value={mobileNumber}
                    onChange={e => setMobileNumber(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>

            { isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
    )

}

/*import {useState, useEffect, useContext} from 'react';

import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

import { Form, Button } from 'react-bootstrap';

export default function Register() {

	const {user} = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	// to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	useEffect(() =>
	{
		if(email!== "" && password1!== "" && password2!== "" && password1 === password2)
		{
			setIsActive(true);
		}
		else
		{
			setIsActive(false);
		}
	},[email, password1, password2])

	// function to simulate user registration
	function registerUser(e)
	{
		e.preventDefault();

		// clear input fields
		setEmail("");
		setPassword1("");
		setPassword2("");

		alert("Thank you for registering!");
	}

    return (
    	(user.email!== null) ?
        <Navigate to="/courses"/>
        :
        <Form onSubmit = {(e) => registerUser(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email"
	                value ={email}
	                onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                value ={password1}
	                onChange={e => setPassword1(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password"
	                value ={password2}
	                onChange={e => setPassword2(e.target.value)} 
	                required
                />
            </Form.Group>
            {isActive ?
	            <Button variant="primary" type="submit" id="submitBtn">
	            	Submit
	            </Button>
	            :
	            <Button variant="danger" type="submit" id="submitBtn" disabled>
	            	Submit
	            </Button>
        	}
        </Form>
    )

}

*/