import bannerData from '../data/bannerData';

import Banner from '../components/Banner'
import Highlights from '../components/Highlights'

export default function Home()
{
	return (
	<>
      <Banner {...bannerData[0]}/>
      <Highlights />
    </>)
}